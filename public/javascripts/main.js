const addButton = document.getElementById('addButton');

let phoneDirectory = [];

const deleteRow = async (id) => {
  const response = await fetch(`/api/contact/${id}`, { method: 'DELETE' });
  await response.text();

  fetchRows();
};

const dbpress = async (addressField,id, name, phone, email,address)=> {
  if(address!= null) {
    addressField.innerHTML='<input id="iaddress" type="text" value="'+address+'" />'
    const a = document.getElementById('iaddress');
    a.focus();
    a.addEventListener("keyup", function(event) {
      event.preventDefault();
      if (event.keyCode === 13) {
        //alert(a.value);
        updateVal(id, name, phone, email,a.value);
        return false;
      }
    });
  }else {
    addressField.innerHTML='<input id="iaddress" type="text" placeholder="Address" />'
    const a = document.getElementById('iaddress');
    a.focus();
    a.addEventListener("keyup", function(event) {
      event.preventDefault();
      if (event.keyCode === 13) {
        updateVal(id, name, phone, email,a.value);
        return false;
      }
    });
  }
}

const updateVal = async (id, name, phone, email,address) => {
  const response = await fetch(`/api/contact/${id}`, {
    method: 'POST',
    body: JSON.stringify({ name, email, phone, address }),
    headers: {
      'Content-Type': 'application/json',
    },
  });
  await response.text();
  fetchRows();
}

const renderRows = () => {
  const tbody = document.getElementById('tbody');
  tbody.innerHTML = '';

  const rows = phoneDirectory.map(({
    id, name, phone, email, address
  }) => {
    const row = document.createElement('tr');

    const nameField = document.createElement('td');
    nameField.innerText = name;
    row.appendChild(nameField);
    const phoneField = document.createElement('td');
    phoneField.innerText = phone;
    row.appendChild(phoneField);
    const emailField = document.createElement('td');
    emailField.innerText = email;
    row.appendChild(emailField);
    const addressField = document.createElement('td');
    addressField.innerText = address;
    addressField.ondblclick = () => dbpress(addressField, id, name, phone, email,address);
    row.appendChild(addressField);
    const buttonField = document.createElement('td');
    const button = document.createElement('button');
    button.innerText = 'Remove';
    button.onclick = () => deleteRow(id);
    buttonField.appendChild(button);
    row.appendChild(buttonField);

    return row;
  });

  rows.forEach((row) => {
    tbody.appendChild(row);
  });
};

const fetchRows = async () => {
  const response = await fetch('/api/contacts');
  const phoneDirectoryArray = await response.json();

  phoneDirectory = [];
  phoneDirectory = phoneDirectoryArray;

  renderRows();
};

const addNewContact = async ({ name, phone, email, address }) => {
  const response = await fetch('/api/contact', {
    method: 'PUT',
    body: JSON.stringify({ name, phone, email, address }),
    headers: {
      'Content-Type': 'application/json',
    },
  });
  const newContact = await response.json();

  phoneDirectory.push(newContact);

  fetchRows();
};

function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function validatephone(phone) {
  var re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
  return re.test(phone);
}

addButton.onclick = () => {
  const iName = document.getElementById('iName');
  const iPhone = document.getElementById('iPhone');
  const iEmail = document.getElementById('iEmail');
  const iaddress = document.getElementById('iaddress');

  const name = iName.value;
  const phone = iPhone.value;
  const email = iEmail.value;
  const address = iaddress.value;
  
if(name.length == 0 || name.value=== "")
{
  iName.placeholder ='Unfilled space';
}if(phone.length == 0 || phone.value === ""){
  iPhone.placeholder ='Unfilled space';
}
if(email.length == 0 || email.value=== ""){
  iEmail.placeholder ='Unfilled space';
}if(address.length == 0 || address.value=== ""){
  iaddress.placeholder ='Unfilled space';
}else {
  if(!validatephone(phone)) {
    iPhone.placeholder= 'Incorrect Number';
  }
  if(!validateEmail(email)){
    iEmail.placeholder= 'Incorrect Email';
  }else {
      iName.value = '';
      iPhone.value = '';
      iEmail.value = '';
      iaddress.value = '';

      iName.placeholder = 'Name';
      iPhone.placeholder = 'Phone';
      iEmail.placeholder = 'Email';
      iaddress.placeholder = 'Adress';
    
      addNewContact({ name, phone, email, address });
  }
}

};

fetchRows();
